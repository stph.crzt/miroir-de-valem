#!/bin/bash

while read -r line; do max=$line; done < filename_index.txt
i=$max
echo $i
while true; do 
	i=$((i-1))
	if (($i==-1))
	 then
		i=$((max-1))
		echo $i
	fi
	./Scripts/open_image.sh "Pictures/pic_${i}.jpg"
	sleep 3

done
