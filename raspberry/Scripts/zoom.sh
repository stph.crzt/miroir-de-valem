#!/bin/bash

case $1 in

  p)xdotool key Up
    xdotool key Up;;

  m)xdotool key Down
    xdotool key Down;;

  m_u_p)xdotool keydown ctrl+Up;;

  m_u_r)xdotool keyup ctrl+Up;;

  m_d_p)xdotool keydown ctrl+Down;;

  m_d_r)xdotool keyup ctrl+Down;;

  m_l_p)xdotool keydown ctrl+Left;;

  m_l_r)xdotool keyup ctrl+Left;;

  m_r_p)xdotool keydown ctrl+Right;;

  m_r_r)xdotool keyup ctrl+Right;;

esac
