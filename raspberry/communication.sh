#!/bin/bash

#lauch after this programm in a  terminal sudo nc -v 192.168.1.1 1025
#you can now send orders to this script

cd /home/pi/Documents/PR/pr_miroir_de_valem/raspberry
./Scripts/connection.sh &


echo "Welcome to the Miroir de Valem"

#get the last index in order to name the new pictures with a new name
while read -r line; do i=$line; done < filename_index.txt

while read -r line; do j=$line; done < command_index.txt


#launch server
coproc nc -k -l 1025

sleep 1

#hide the command window and move the mouse out of the taskbar
xdotool windowminimize $(xdotool getactivewindow)
xdotool mousemove 500 500

#wait for orders
while read -r cmd; do

	#split string received into an array
	IFS="|" read -ra cmd2 <<<"$cmd"

	case ${cmd2[0]} in

		#if  p is received, take a picture and launch pixelize
		p)killall home.sh
			./Scripts/miroir_de_Valem
			echo "p_ok";;

		#if d is received, show the drawing on the screen
		d)killall home.sh
			FILE="Impressions/${cmd2[1]}"
			./Scripts/open_image.sh $FILE;;

		#if m is received, show the mosaic on the screen
		m)killall home.sh
			FILE="Pictures/${cmd2[1]}"
			./Scripts/open_image.sh $FILE;;

		#if s is received, copy the last mosaic into the Pictues Folder, with its new name
		s)killall home.sh
			FILENAME="pic_${i}.jpg"
			cp  TemporaryFiles/output.jpeg "TemporaryFiles/${FILENAME}"
			cp "TemporaryFiles/${FILENAME}"  Pictures
			rm "TemporaryFiles/${FILENAME}"
			rm filename_index.txt
			i=$((i+1))
			echo $i>> filename_index.txt

			#create preview from the mosaic
			convert "Pictures/${FILENAME}" -scale 400 "Preview/${FILENAME}"
			echo "s_ok|$FILENAME";;

		#if c_d is received write the command in the commandFile and copy the drawing into the CommandeDessin folder
		#if commande = name, num or paper, email adress, file, format
		c_d) killall home.sh
			echo "${cmd2[1]}, ${cmd2[2]}, ${cmd2[3]}, ${cmd2[4]}, ${cmd2[5]}, ${cmd2[6]}">> commandesDessins.csv
			cp "Impressions/${cmd2[4]}" CommandeDessin/;;

		#if c_m is received write  the  command in the commandesReflet File and copy mosaic file into CommandeReflet Folder
		c_m) killall home.sh
			cmdName="cmdMosaic_$((j))"
			cp TemporaryFiles/output.jpeg "TemporaryFiles/${cmdName}"
			cp "TemporaryFiles/${cmdName}"	 CommandeReflet/
			echo "${cmd2[1]}, ${cmd2[2]}, ${cmd2[3]}, ${cmdName}, ${cmd2[5]}, ${cmd2[6]} ">> commandesReflets.csv
			rm "TemporaryFiles/${cmdName}"
			j=$((j+1))
            rm command_index.txt
			echo j>> command_index.txt;;

		#if n is received, save comment in a file text
		n) killall home.sh
			echo "${cmd2[1]}" >> comments.csv;;

		#kill home script
		stop) killall home.sh
		killall feh
		./Scripts/open_image.sh blank.jpg;;

		#displays a new mosaic every 5 seconds on the screen
		h) ./Scripts/home.sh &
			./Scripts/backup.sh;;

		#Zoom and move zoom zone
		z) ACTION="${cmd2[1]}"
			./Scripts/zoom.sh $ACTION;;

		q) break;;

		*) echo 'What ?';;
	esac
done <&"${COPROC[0]}" >&"${COPROC[1]}"

#kill server
kill "$COPROC_PID"
