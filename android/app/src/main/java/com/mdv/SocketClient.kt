package com.mdv

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket

abstract class SocketClient : AppCompatActivity(), View.OnClickListener {

    private var mTextViewReplyFromServer: TextView? = null
//    private val address = "192.168.1.1" // Valeur Raspberry
    private val address = "192.168.43.97" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                output.println(msg)
                output.flush()

                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val st = input.readLine()
                Log.d("Client", st)
                handler.post {
                    Log.d("Server", "ANSWER")
                    val s = mTextViewReplyFromServer!!.text.toString()
                    if (st.trim { it <= ' ' }.length != 0) mTextViewReplyFromServer!!.text =
                        "$s\nFrom Server : $st"
                    Log.d(
                        "Server response",
                        mTextViewReplyFromServer!!.text.toString()
                    )
                }
                output.close()
                Log.d("End", "Output closed")
                out.close()
                Log.d("End", "Out closed")
                s.close()
                Log.d("End", "Socket closed")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}