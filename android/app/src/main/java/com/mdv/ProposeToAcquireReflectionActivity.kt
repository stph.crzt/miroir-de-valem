package com.mdv

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.p8.*

class ProposeToAcquireReflectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.p8)

        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()

        // Terminer

        button10.setOnClickListener {
            if ((checkBox.isChecked && checkBox2.isChecked) || (!checkBox.isChecked && !checkBox2.isChecked)) return@setOnClickListener
            else if (checkBox.isChecked) {
                // TODO
                val intent = Intent(this, ChooseSupportActivity::class.java)

                startActivity(intent)
            } else {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }
        }


        // Home Button
        imageButton20.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton19.setOnClickListener{
            finish()
        }

    }
}
