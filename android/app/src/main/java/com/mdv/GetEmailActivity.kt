package com.mdv

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.email_adress.*
import kotlinx.android.synthetic.main.propose_to_save_activity.*

class GetEmailActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.email_adress)

        val intent_sent = intent
        val paper = intent_sent.getStringExtra("paper")
        val numeric = intent_sent.getStringExtra("numeric")
        Log.d("paper_received", paper)
        Log.d("numeric_received", numeric)

       val showButton = findViewById<Button>(R.id.button12)
        val editText = findViewById<EditText>(R.id.editText2)

        showButton.setOnClickListener {
            if (paper!="none") {
                val text = editText.text
                val email = text.toString()

                val intent = Intent(this, GetInfosActivity::class.java)
                val msg = numeric + "$email"
                intent.putExtra("numeric", msg)
                intent.putExtra("paper", paper)
                Log.d("msg_sent_email", msg)
                Log.d("msg_sent_email2", paper)
                startActivity(intent)
            }
            else if (paper=="none") {
                val text = editText.text
                val email = text.toString()

                val intent = Intent(this, CommandSavedActivity::class.java)
                val msg = numeric + "$email"
                intent.putExtra("numeric", msg)
                intent.putExtra("paper", paper)
                Log.d("msg_sent_email", msg)
                Log.d("msg_sent_email2", paper)
                startActivity(intent)
            }
        }

        // Laisser un commentaire
        imageButton11.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton33.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton17.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton13.setOnClickListener{
            finish()
        }

    }
}
