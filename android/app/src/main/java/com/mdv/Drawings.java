package com.mdv;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.File;

public class Drawings implements Parcelable {

    private int mUrl;
    private String mTitle;

    public Drawings(int url, String title) {
        mUrl = url;
        mTitle = title;
    }

    protected Drawings(Parcel in) {
        mUrl = in.readInt();
        mTitle = in.readString();
    }

    public static final Creator<Drawings> CREATOR = new Creator<Drawings>() {
        @Override
        public Drawings createFromParcel(Parcel in) {
            return new Drawings(in);
        }

        @Override
        public Drawings[] newArray(int size) {
            return new Drawings[size];
        }
    };

    public int getUrl() {
        return mUrl;
    }

    public void setUrl(int  url) {
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public static  Drawings[] getDrawings() {

        /*Context context = parent.getContext;
        String path = Environment.(
                Environment.DIRECTORY_DOWNLOADS);

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS); */

       // new Drawings( path.getAbsolutePath(), "img_04.jpg"),
      // Log.d("drawings",path);

        return new Drawings[]{
                new Drawings(R.drawable.b_2,"b_2"),
                new Drawings(R.drawable.b_3, "b_3"),
                new Drawings(R.drawable.b_4, "b_4"),
                new Drawings(R.drawable.b_5, "b_5"),
                new Drawings(R.drawable.b_6, "b_6"),
                new Drawings(R.drawable.b_7, "b_7"),
                new Drawings(R.drawable.b_8, "b_8"),
                new Drawings(R.drawable.b_9, "b_9"),
                new Drawings(R.drawable.b_10, "b_10"),
                new Drawings(R.drawable.b_11, "b_11"),
                new Drawings(R.drawable.b_12, "b_12"),
                new Drawings(R.drawable.b_13, "b_13"),
                new Drawings(R.drawable.b_14, "b_14"),
                new Drawings(R.drawable.b_15, "b_15"),
                new Drawings(R.drawable.b_16, "b_16"),
                new Drawings(R.drawable.b_17, "b_17"),
                new Drawings(R.drawable.b_18, "b_18"),
                new Drawings(R.drawable.b_19, "n_19"),
                new Drawings(R.drawable.b_20, "b_20"),
                new Drawings(R.drawable.b_21, "b_21"),
                new Drawings(R.drawable.b_22, "b_22"),
                new Drawings(R.drawable.b_23, "b_23"),
                new Drawings(R.drawable.b_24, "b_24"),
                new Drawings(R.drawable.b_25, "b_25"),
                new Drawings(R.drawable.b_26, "b_26"),
                new Drawings(R.drawable.b_27, "b_27"),
                new Drawings(R.drawable.b_28, "b_28"),
                new Drawings(R.drawable.b_29, "b_29"),
                new Drawings(R.drawable.b_30, "b_30"),
                new Drawings(R.drawable.b_31, "b_31"),
                new Drawings(R.drawable.b_32, "b_32"),
                new Drawings(R.drawable.b_33, "b_33"),
                new Drawings(R.drawable.b_34, "b_34"),
                new Drawings(R.drawable.b_35, "b_35"),
                new Drawings(R.drawable.b_36, "b_36"),
                new Drawings(R.drawable.b_37, "b_37"),
                new Drawings(R.drawable.b_38, "b_38"),
                new Drawings(R.drawable.b_39, "b_39"),
                new Drawings(R.drawable.b_40, "b_40"),
                new Drawings(R.drawable.b_41, "b_41"),
                new Drawings(R.drawable.b_42, "b_42"),
                new Drawings(R.drawable.b_43, "b_43"),
                new Drawings(R.drawable.b_44, "b_44"),
                new Drawings(R.drawable.b_45, "b_45"),
                new Drawings(R.drawable.b_46, "b_46"),
                new Drawings(R.drawable.b_47, "b_47"),
                new Drawings(R.drawable.b_48, "b_48"),
                new Drawings(R.drawable.b_49, "b_49"),
                new Drawings(R.drawable.b_50, "b_50"),
                new Drawings(R.drawable.b_51, "b_51"),
                new Drawings(R.drawable.b_52, "b_52"),
                new Drawings(R.drawable.b_53, "b_53"),
                new Drawings(R.drawable.b_54, "b_54"),
                new Drawings(R.drawable.b_55, "b_55"),
                new Drawings(R.drawable.b_56, "b_56"),
                new Drawings(R.drawable.b_57, "b_57"),
                new Drawings(R.drawable.b_58, "b_58"),
                new Drawings(R.drawable.b_59, "b_59"),
                new Drawings(R.drawable.b_60, "b_60"),
                new Drawings(R.drawable.b_61, "b_61"),
                new Drawings(R.drawable.b_62, "b_62"),
                new Drawings(R.drawable.b_63, "b_63"),
                new Drawings(R.drawable.b_64, "b_64"),
                new Drawings(R.drawable.g1_1, "g1_1"),
                new Drawings(R.drawable.g1_2, "g1_2"),
                new Drawings(R.drawable.g2_1, "g2_1"),
                new Drawings(R.drawable.g2_2, "g2_2"),
                new Drawings(R.drawable.g3_1, "g3_1"),
                new Drawings(R.drawable.g3_2, "g3_2"),
                new Drawings(R.drawable.g3_3, "g3_3"),
                new Drawings(R.drawable.g3_4, "g3_4"),
                new Drawings(R.drawable.g4_1, "g4_1"),
                new Drawings(R.drawable.g4_2, "g4_2"),
                new Drawings(R.drawable.g4_3a, "g4_3_a"),
                new Drawings(R.drawable.g4_4, "g4_4"),
                new Drawings(R.drawable.g4_5, "g4_5"),
                new Drawings(R.drawable.n_1, "n_1"),
                new Drawings(R.drawable.n_2, "n_2"),
                new Drawings(R.drawable.n_3, "n_3"),
                new Drawings(R.drawable.n_4, "n_4"),
                new Drawings(R.drawable.n_5, "n_5"),
                new Drawings(R.drawable.n_6, "n_6"),
                new Drawings(R.drawable.n_7, "n_7"),
                new Drawings(R.drawable.n_8, "n_8"),
                new Drawings(R.drawable.n_9, "n_9"),
                new Drawings(R.drawable.n_10, "n_10"),
                new Drawings(R.drawable.n_11, "n_11"),
                new Drawings(R.drawable.n_12, "n_12"),
                new Drawings(R.drawable.n_13, "n_13"),
                new Drawings(R.drawable.n_14, "n_14"),
                new Drawings(R.drawable.n_15, "n_15"),
                new Drawings(R.drawable.n_16, "n_16"),


        };
    }

    //Uri myURI = Uri.parse("android.resource://com.example.project/" + R.drawable.myimage);

    public static Uri getUrI(int res){
        return Uri.parse("android.resource://com.example.mdv/" + res);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mUrl);
        parcel.writeString(mTitle);
    }
}