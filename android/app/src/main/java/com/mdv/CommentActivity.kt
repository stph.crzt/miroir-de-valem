package com.mdv

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.comment_activity.*

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket
import android.text.style.RelativeSizeSpan
import android.text.SpannableStringBuilder


class CommentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.comment_activity)

        val showButton = findViewById<Button>(R.id.Send_comment)
        val editText = findViewById<EditText>(R.id.simpleEditText)
        //  todo modifier le nb max de char dans le xml

        showButton.setOnClickListener {
            val text = editText.text
            val message = text.toString()
            val msg = "n|" + "$message"
            Log.d("msg sent",msg)
            sendMessage(msg)

            val biggerText = SpannableStringBuilder("Le commentaire a bien été enregistré")
            biggerText.setSpan(RelativeSizeSpan(1.35f), 0, biggerText.length, 0)
            Toast.makeText(this, biggerText, Toast.LENGTH_LONG).show()
            finish()
        }

        //Home Button
        imageButton28.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton27.setOnClickListener{
            finish()
        }
    }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                    //Replace below IP with the IP of that device in which server socket open.
                    //If you change port then change the port number in the server side code also.

                    Log.d("Socket Client", "sendMessage() initiated")
                    Log.d("Socket Client Address", address)
                    Log.d("Socket Client Port", port.toString())
                    Log.d("Socket Client Message", msg)

                    // Initialisation de l'objet socket
                    val s = Socket(address, port)
                    val out = s.getOutputStream()
                    val output = PrintWriter(out)
                    // Envoi de la requête sur socket
                    output.println(msg)
                    output.flush()

                    // On lit la réponse du serveur
                    val input = BufferedReader(InputStreamReader(s.getInputStream()))
                    Log.d("Client", "En attente de réponse")
                    val response = input.readLine()
                    Log.d("Server Response", response)

                    // Réponse nulle
                    if(response == null || response.isEmpty() || response == ""){
                        Log.e("Server Error", "Response null or empty")
                        throw Exception("NullResponse")
                    }
                    // Mauvaise réponse
                    else if(response != "n_ok"){
                        Log.e("Server Error", "Bad response")
                        throw Exception("BadResponse")
                    }
                    // Retour = n_ok => Le commentaire s'est bien envoyé
                    handler.post {
                        //val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
                        // startActivity(intent) // TODO quel comportement si l'envoi s'est bien déroulé
                    }

                    output.close()
                    Log.d("End", "Output closed")

                    out.close()
                    Log.d("End", "Out closed")

                    s.close()
                    Log.d("End", "Socket closed")

                } catch (e: IOException) {
                    e.printStackTrace()
            }
        })
        thread.start()
    }

}
