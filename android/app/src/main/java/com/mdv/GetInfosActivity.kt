package com.mdv

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.personnal_infos.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket

class GetInfosActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personnal_infos)

        val intent_sent = intent
        val paper = intent_sent.getStringExtra("paper")
        val numeric = intent_sent.getStringExtra("numeric")
        Log.d("paper_received", paper)
        Log.d("numeric_received", numeric)

        val showButton = findViewById<Button>(R.id.button12)
        val editText_adress = findViewById<EditText>(R.id.editText)
        val editText_adress2 = findViewById<EditText>(R.id.editText2)
        val editText_country = findViewById<EditText>(R.id.editText3)
        val editText_name = findViewById<EditText>(R.id.editText4)
        val editText_sirnmae = findViewById<EditText>(R.id.editText5)
        val editText_city = findViewById<EditText>(R.id.editText6)
        val editText_postcode = findViewById<EditText>(R.id.editText7)

        showButton.setOnClickListener {
            val name = editText_name.text.toString()
            val sirname = editText_sirnmae.text.toString()
            val adress = editText_adress.text.toString()
            val adress_2 = editText_adress2.text.toString()
            val city = editText_city.text.toString()
            val country = editText_country.text.toString()
            val postcode = editText_postcode.text.toString()

            val intent = Intent(this , CommandSavedActivity::class.java )
            val msg = paper + "$name|" + "$sirname|" + "$adress $adress_2|" + "$city|" + "$country|" + "$postcode"
            intent.putExtra("numeric",numeric)
            intent.putExtra("paper",msg)
            Log.d("msg_sent_infos",msg)
            Log.d("msg_sent_infos2",numeric)
            startActivity(intent)
        }

        // Laisser un commentaire
        imageButton11.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton33.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton17.setOnClickListener{
            sendMessage("stop")
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton13.setOnClickListener{
            finish()
        }

    }
    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
                // Mauvaise réponse
                else if(response != "p_ok" && response != "stop_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                // Retour = p_ok => Pixelize s'est bien déroulé
                handler.post {
                    //val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
                    // startActivity(intent) // TODO quel comportement si l'affichage s'et bien déorulé
                }

                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}